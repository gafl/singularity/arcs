#distribution based on: debian 9.5
Bootstrap:docker
From:debian:9.5-slim

# container for arcs 
# Scaffolding genome sequence assemblies using 10X Genomics GemCode/Chromium data
# Build:
# sudo singularity build arcs_v1.1.0.simg Singularity.arcs_v1.1.0

%environment
export LC_ALL=C
export LC_NUMERIC=en_GB.UTF-8
export PATH="/opt/miniconda/bin:$PATH"

#%labels
#AUTHOR Jacques Lagnel
#VERSION 1.1
#LICENSE MIT
#DATE_MODIF 04/07/2019
#PACKAGE ema (v0.6.2)

%help
Container for arcs scaffolder 
Scaffolding genome sequence assemblies using 10X Genomics GemCode/Chromium data
https://github.com/bcgsc/arcs

Version: arcs Version: 1.1.0
Package installation using Miniconda3 V4.7.12
All packages are in /opt/miniconda/bin & are in PATH

Default runscript: arcs

Usage:
    arcs_v1.1.0.simg --help
    or:
    singularity exec arcs_v1.1.0.simg arcs --help


%runscript
    #default runscript: arcs passing all arguments from cli: $@
    exec /opt/miniconda/bin/arcs "$@"

%post

    #essential stuff but minimal
    apt update
    #for security fixe:
    apt upgrade -y
    apt install -y wget bzip2 build-essential make zlib1g-dev

    #install conda
    cd /opt
    rm -fr miniconda

    #miniconda3: get miniconda3 version 4.7.12
    wget https://repo.continuum.io/miniconda/Miniconda3-4.7.12-Linux-x86_64.sh -O miniconda.sh

    #install conda
    bash miniconda.sh -b -p /opt/miniconda
    export PATH="/opt/miniconda/bin:$PATH"

    #add channels  
    conda config --add channels defaults
    conda config --add channels conda-forge
    conda config --add channels bioconda

    #install arcs
    conda install -c bioconda arcs=1.1.0

    #cleanup    
    conda clean -y --all
    rm -f /opt/miniconda.sh
    apt autoremove --purge
    apt clean

